# Default VPC Deletion(AWS)
This python project is a short script to delete the default VPC and its related resources from an Amazon Web Services(AWS) account.

# Motivation
Deleting the default VPC from an AWS account should always be one of the fist actions taken when creating an AWS account. The reasoning behind this is due to the lack of standard security logs such as VPC flow logs, wide open network access control lists(NACLs), absent tagging, and provides problem for automation tools such as cloudformation where ad efautl VPC wil be assumes when deploying new infrastructure if a VPC id is not provided. By removing the default VPC as soon as possible, a new secure infrastructure can be put in place to increase teh value of logging data, secure networking, inventory and cost optimization, and constistent baseline for additional infrastrcuture.

# Installation
To install the Python script simple clone this repository then ensure the folling technology and requirements are fullfilled.

## Technologies
- Python3.6+
- AWS
- AWSCLI
- Python Boto3 AWS SDK

## Requirements
- AWS Account with existing default VPC
- Python 3.6+
- AWSCLI Installed and configured
- AWS Iam user or SAML role with administrative privileges
- AWS Profile configured from AWSCLI(Default Profile)

# Usage
To use the python script first a few conditions must be met to delete your default VPC.

1. Ec2 instances must be terminated or Migrated to a new VPC
2. Network interfaces must be detached and deleted
3. Additional Security groups that are not the original "default" security group must be deleted
4. RDS relate security groups and subnets must be terminated if applicable
5. VPC endpoints must be removed
6. Removal/termination of other AWS service that could potentially be using the VPC for tenancy by tracking the defautl VPC, Security groups and Subnet Ids

If the VPC exists with in fresh AWS account, none of the conditions above will apply.


Once all pre-requisite conditions have been met, simply execute the python script from within the directory
```
python3 /<DIRECTORY PATH>/default-vpc-deletion/main.py
```

# Changelog
## **1.0.1(2019-11-23)**
**Implemented Enhancements:**
- Basic functionality of python script to remove default vpc from a clean/fresh AWS Account d1226697df272546ce374f009924492dbd346972