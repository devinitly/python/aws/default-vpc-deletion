#!/usr/bin/env python3
import boto3
import logging


def delete_default_vpc_resources():
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)

    client = boto3.client('ec2')

    # Get IDs of all default vpc related and dependant resources
    logger.info('GATHERING FACTS...')

    def get_default_vpc():
        logger.info('RETRIEVING DEFAULT VPC ID...')
        vpc = client.describe_vpcs(
            Filters=[
                {
                    'Name': 'isDefault',
                    'Values': ['true']
                }
            ]
        )

        for default_vpc in vpc['Vpcs']:
            return default_vpc['VpcId']

    default_vpc_id = get_default_vpc()
    logger.info('DEFAULT VPC ID: {}'.format(default_vpc_id))

    def get_internet_gateway(vpc):
        logger.info('RETRIEVING DEFAULT INTERNET GATEWAY ID...')
        internet_gateway = client.describe_internet_gateways(
            Filters=[
                {
                    'Name': 'attachment.vpc-id',
                    'Values': [vpc]
                }
            ]
        )
        for default_internet_gateway in internet_gateway['InternetGateways']:
            logger.info('DEFAULT INTERNET GATEWAY ID: {}'.format(default_internet_gateway['InternetGatewayId']))
            return default_internet_gateway['InternetGatewayId']

    def get_route_table(vpc):
        logger.info('RETRIEVING DEFAULT ROUTE TABLE ID...')
        route_table = client.describe_route_tables(
            Filters=[
                {
                    'Name': 'vpc-id',
                    'Values': [vpc]
                }
            ]
        )

        for table in route_table['RouteTables']:
            for association in table['Associations']:
                logger.info('DEFAULT ROUTE TABLE ID: {}'.format(association['RouteTableId']))
                return association['RouteTableId']

    def get_subnets():
        logger.info('RETRIEVING DEFAULT SUBNET ID\'S...')
        all_subnets = client.describe_subnets(
            Filters=[
                {
                    'Name': 'default-for-az',
                    'Values': ['true']

                }
            ]
        )

        subnets = []
        for subnet in all_subnets['Subnets']:
            subnets.append(subnet['SubnetId'])

        logger.info('DEFAULT SUBNET ID\'S: {}'.format(subnets))
        return subnets

    default_internet_gateway_id = get_internet_gateway(default_vpc_id)
    default_subnets = get_subnets()
    default_route_table = get_route_table(default_vpc_id)

    logger.info('NECESSARY FACTS GATHERED!')
    logger.info('DELETING RESOURCES...')

    # Start deleting resources
    def delete_default_internet_gateway(igw, vpc):
        logger.info('DETACHING INTERNET GATEWAY: {} FROM VPC: {}...'.format(igw, vpc))
        client.detach_internet_gateway(
            InternetGatewayId=igw,
            VpcId=vpc
        )
        logger.info('DELETING INTERNET GATEWAY: {}'.format(igw))
        client.delete_internet_gateway(
            InternetGatewayId=igw
        )
        logger.info('INTERNET GATEWAY: {} DELETED!'.format(igw))

    def delete_default_subnets(subnets):
        logger.info('DELETING DEFAULT SUBNETS {}...'.format(subnets))
        for subnet in subnets:
            client.delete_subnet(
                SubnetId=subnet
            )
            logger.info('SUBNET: {} DELETED!'.format(subnet))

    def delete_default_route_table_routes(table):
        logger.info('DELETING DEFAULT ROUTE TABLE: {}...'.format(table))
        logger.info('DELETING DEFAULT ROUTE TABLE ROUTES...')

        client.delete_route(
            RouteTableId=table,
            DestinationCidrBlock='0.0.0.0/0'
        )
        logger.info('ROUTE 0.0.0.0/0 DELETED!')

    def delete_default_vpc(vpc):
        logger.info('DELETING DEFAULT VPC: {}...'.format(vpc))
        client.delete_vpc(
            VpcId=vpc
        )
        logger.info('DEFAULT VPC: {} DELETED!'.format(vpc))

    delete_default_internet_gateway(default_internet_gateway_id, default_vpc_id)
    delete_default_subnets(default_subnets)
    delete_default_route_table_routes(default_route_table)
    delete_default_vpc(default_vpc_id)


def main():
    delete_default_vpc_resources()


if __name__ == '__main__':
    main()
